package matrix;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.tcb.matrix.Matrix;
import com.tcb.matrix.SparseMatrix;


public class SparseMatrixTest {

	protected static double[][] data = new double[][]{
		{0,1,2},
		{3,4,5},
		{6,7,8}
	};
	
	protected Matrix m;

	@Before
	public void setUp() throws Exception {
		this.m = createMatrix();
	}
	
	protected Matrix createMatrix(){
		return new SparseMatrix(data);
	}
	
	public static void assertDataEquals(double[][] ref, double[][] test){
		assertEquals(ref.length, test.length);
		assertEquals(ref[0].length, test[0].length);
		for(int i=0;i<ref.length;i++){
			for(int j=0;j<ref[0].length;j++){
				assertEquals(ref[i][j], test[i][j], 0.01);
			}
		}
	}

	@Test
	public void testSparseMatrixDoubleArrayArray() {
		assertDataEquals(data,m.getData());
	}

	@Test
	public void testGetData() {
		testSparseMatrixDoubleArrayArray();
	}

	@Test
	public void testCopy() {
		Matrix copy = m.copy();
		assertDataEquals(m.getData(), copy.getData());
		assertFalse(m.getData()==copy.getData());
	}

	@Test
	public void testGet() {
		assertEquals(0, m.get(0, 0), 0.01);
		assertEquals(4, m.get(1, 1), 0.01);
	}

	@Test
	public void testSet() {
		assertEquals(4, m.get(1, 1), 0.01);
		m.set(1, 1, -1d);
		assertEquals(-1, m.get(1, 1), 0.01);
	}

	@Test
	public void testAdd() {
		Matrix m2 = m.copy();
		m2.add(m);
		assertDataEquals(data, m.getData());
		assertEquals(2*data[1][1], m2.get(1, 1), 0.01);
		assertEquals(2*data[2][1], m2.get(2, 1), 0.01);
	}

	@Test
	public void testSubtract() {
		Matrix m2 = m.copy();
		m2.subtract(m);
		assertDataEquals(data, m.getData());
		assertEquals(0, m2.get(1, 1), 0.01);
		assertEquals(0, m2.get(2, 1), 0.01);
	}

	@Test
	public void testScalarMultiply() {
		Matrix m2 = m.copy();
		m2.scalarMultiply(5);
		assertDataEquals(data, m.getData());
		assertEquals(5*data[1][1], m2.get(1, 1), 0.01);
		assertEquals(5*data[2][1], m2.get(2, 1), 0.01);
	}

	@Test
	public void testGetFrobeniusNorm() {
		double norm = m.getFrobeniusNorm();
		assertEquals(14.28,norm, 0.01);
	}
	
	@Test
	public void testGetRow(){
		double[] row = m.getRow(1);
		double[] ref = new double[]{3,4,5};
		
		for(int i=0;i<ref.length;i++){
			assertEquals(ref[i],row[i],0.01);
		}
		
	}
	
	@Test
	public void testGetZeroIfUnassigend(){
		Matrix testMatrix = new SparseMatrix(3,3);
		assertEquals(0d, testMatrix.get(1, 1), 0.01);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testWillFailForTooBigRow(){
		m.get(3, 0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testWillFailForTooBigColumn(){
		m.get(0, 3);
	}

}
