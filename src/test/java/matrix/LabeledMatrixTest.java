package matrix;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tcb.matrix.LabeledMatrix;
import com.tcb.matrix.LabeledSquareMatrixImpl;
import com.tcb.matrix.Matrix;
import com.tcb.matrix.SparseMatrix;

public class LabeledMatrixTest {

	
	protected static double[][] data = new double[][]{
		{0,1,2},
		{3,4,5},
		{6,7,8}
	};
	
	private List<String> labels;
	private LabeledMatrix<String> labeledMatrix;

	@Before
	public void setUp() throws Exception {
		this.labels = Arrays.asList("a","b","c");
		this.labeledMatrix = createMatrix();
	}
	
	protected LabeledMatrix<String> createMatrix(){
		Matrix sparseMatrix = new SparseMatrix(data);
		return LabeledSquareMatrixImpl.create(labels,sparseMatrix);
	}

	@Test
	public void testGetStringString() {
		assertEquals(3.0,labeledMatrix.get("b", "a"),0.01);
	}

	@Test
	public void testSetStringStringDouble() {
		assertEquals(3.0,labeledMatrix.get("b", "a"),0.01);
		labeledMatrix.set("b", "a", -2.0);
		assertEquals(-2.0,labeledMatrix.get("b", "a"),0.01);
	}

}
