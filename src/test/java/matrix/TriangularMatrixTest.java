package matrix;


import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.tcb.matrix.Matrix;
import com.tcb.matrix.TriangularMatrix;

public class TriangularMatrixTest {

	private TriangularMatrix m;
	private int dim;
	private double[][] data = 
				{{0.0,1.0,2.0,3.0},
				{1.0,4.0,5.0,6.0},
				{2.0,5.0,7.0,8.0},
				{3.0,6.0,8.0,9.0}};

	@Before
	public void setUp() throws Exception {
		double counter = 0;
		this.dim = 4;
		this.m = new TriangularMatrix(4);
		for(int i=0;i<dim;i++){
			for(int j=i;j<dim;j++){
				m.set(i, j, counter);
				counter++;
			}
		}
		
	}
	
	public static void assertDataEquals(
			Double[] ref, Double[] test){
		assertEquals(ref.length, test.length);
		for(int i=0;i<ref.length;i++){
				assertEquals(ref[i], test[i], 0.01);
		}
	}
	
	@Test
	public void testGet() {
		assertEquals(0.0,m.get(0, 0), 0.01);
		assertEquals(2.0,m.get(0, 2), 0.01);
		assertEquals(8.0,m.get(2, 3), 0.01);
		assertEquals(9.0,m.get(3, 3), 0.01);
		assertEquals(2.0,m.get(2, 0), 0.01);
		assertEquals(8.0,m.get(3, 2), 0.01);
	}

	@Test
	public void testSet() {
		assertEquals(8., m.get(2, 3), 0.01);
		m.set(2, 3, 10.);
		assertEquals(10., m.get(2, 3), 0.01);
		assertEquals(10., m.get(3, 2), 0.01);
	}
	
	@Test
	public void testGetDim() {
		assertEquals(4, m.getRowCount());
		assertEquals(4, m.getColumnCount());
	}
	
	@Test
	public void testGetData(){
		Assert.assertArrayEquals(data, m.getData());
	}

	@Test
	public void testApply() {
		m.apply((d) -> 2*d);
		
		assertEquals(0.0,m.get(0, 0), 0.01);
		assertEquals(4.0,m.get(0, 2), 0.01);
		assertEquals(16.0,m.get(2, 3), 0.01);
		assertEquals(18.0,m.get(3, 3), 0.01);
		assertEquals(4.0,m.get(2, 0), 0.01);
		assertEquals(16.0,m.get(3, 2), 0.01);
	}
		
	@Test
	public void testCopy() {
		Matrix copy = m.copy();
		Assert.assertArrayEquals(m.getData(), copy.getData());
		assertFalse(m.getData()==copy.getData());
	}
	
	@Test
	public void testAdd() {
		Matrix m2 = m.copy();
		m2.add(m);
		Assert.assertArrayEquals(data, m.getData());
		assertEquals(2*data[1][1], m2.get(1, 1), 0.01);
		assertEquals(2*data[2][1], m2.get(1, 2), 0.01);
	}

	@Test
	public void testSubtract() {
		Matrix m2 = m.copy();
		m2.subtract(m);
		Assert.assertArrayEquals(data, m.getData());
		assertEquals(0, m2.get(1, 1), 0.01);
		assertEquals(0, m2.get(1, 2), 0.01);
	}

	@Test
	public void testScalarMultiply() {
		Matrix m2 = m.copy();
		m2.scalarMultiply(5);
		Assert.assertArrayEquals(data, m.getData());
		assertEquals(5*data[1][1], m2.get(1, 1), 0.01);
		assertEquals(5*data[2][1], m2.get(1, 2), 0.01);
		assertEquals(5*data[2][1], m2.get(2, 1), 0.01);
	}

	@Test
	public void testGetFrobeniusNorm() {
		double norm = m.getFrobeniusNorm();
		assertEquals(20.59,norm, 0.01);
	}
	
	

}
