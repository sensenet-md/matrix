package com.tcb.matrix;

import java.util.function.Function;

public interface Matrix {
	
	public double[][] getData();
	public Matrix copy();
	public double get(int i, int j);
	public void set(int i, int j, double value);
	public void add(Matrix other);
	public void subtract(Matrix other);
	public void scalarMultiply(double factor);
	public double getFrobeniusNorm();
	public double[] getRow(int row);
	public int getRowCount();
	public int getColumnCount();
	
	public void apply(Function<Double,Double> f);
}