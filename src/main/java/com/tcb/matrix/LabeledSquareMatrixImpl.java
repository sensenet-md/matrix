package com.tcb.matrix;

import java.util.List;
import java.util.Map;

import com.tcb.common.util.SafeMap;

public class LabeledSquareMatrixImpl<T> implements LabeledMatrix<T> {

	private Map<T,Integer> labelToIndexMap;
	private Map<Integer,T> indexToLabelMap;
	private Matrix matrix;
	
	
	public static <T> LabeledSquareMatrixImpl<T> create(List<T> labels, Matrix matrix){
		final int labelSize = labels.size();
		if(labelSize!=matrix.getRowCount() || labelSize!=matrix.getColumnCount()){
			throw new IllegalArgumentException("Label size differs from matrix size");
		}
		Map<T,Integer> labelToIndexMap = new SafeMap<>();
		Map<Integer,T> indexToLabelMap = new SafeMap<>();
		for(int i=0;i<labelSize;i++){
			T label = labels.get(i);
			labelToIndexMap.put(label, i);
			indexToLabelMap.put(i, label);
		}
		if(labelSize!=labelToIndexMap.size()) throw new IllegalArgumentException(
				"Labels are not unique");
		return new LabeledSquareMatrixImpl<>(labelToIndexMap,indexToLabelMap,matrix);
	}
	
	public static <T> LabeledSquareMatrixImpl<T> create(List<T> labels, LabeledMatrix<?> matrix){
		return create(labels,matrix.getMatrix());
	}
	
	private LabeledSquareMatrixImpl(Map<T,Integer> labelToIndexMap, Map<Integer,T> indexToLabelMap, Matrix matrix){
		this.labelToIndexMap = labelToIndexMap;
		this.indexToLabelMap = indexToLabelMap;
		this.matrix = matrix;
	}
	
	@Override
	public Double get(T a, T b) {
		Integer aIdx = getIndex(a);
		Integer bIdx = getIndex(b);
		return matrix.get(aIdx, bIdx);
	}

	@Override
	public T getLabel(Integer index) {
		return indexToLabelMap.get(index);
	}

	@Override
	public Integer getIndex(T a) {
		return labelToIndexMap.get(a);
	}

	@Override
	public void set(T a, T b, double value) {
		Integer aIdx = getIndex(a);
		Integer bIdx = getIndex(b);
		matrix.set(aIdx, bIdx, value);		
	}

	@Override
	public Matrix getMatrix() {
		return matrix;
	}

	@Override
	public int getRowCount() {
		return matrix.getRowCount();
	}

	@Override
	public int getColumnCount() {
		return matrix.getColumnCount();
	}

}
