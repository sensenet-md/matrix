package com.tcb.matrix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.commons.math3.linear.OpenMapRealMatrix;

import com.tcb.common.util.Tuple;

public class SparseMatrix implements Matrix {
	private final Map<Integer,Map<Integer,Double>> map;
	private final int rows;
	private final int columns;
	
	public SparseMatrix(int rows, int columns){
		this(	rows,
				columns,
				new HashMap<Integer,Map<Integer,Double>>()
			);
	}
	
	protected SparseMatrix(int rows, int columns, Map<Integer,Map<Integer,Double>> map){
		this.rows = rows;
		this.columns = columns;
		this.map = map;
	}
	
	public SparseMatrix(double[][] values){
		this(
				values.length,
				values[0].length
			);
		final int rows = values.length;
		final int columns = values[0].length;
		for(int i=0;i<rows;i++) {
			for(int j=0;j<columns;j++){
				set(i,j,values[i][j]);
			}
		}
	}
	
	@Override
	public double[][] getData(){
		double[][] data = new double[rows][columns];
		for(int i=0;i<rows;i++){
			for(int j=0;j<columns;j++){
				data[i][j] = get(i,j);
			}
		}
		return data;
	}
			
	@Override
	public Matrix copy(){
		Matrix m = new SparseMatrix(rows,columns);
		for(Integer i:map.keySet()){
			Map<Integer,Double> column = map.get(i);
			for(Integer j:column.keySet()){
				m.set(i, j, get(i,j));
			}
		}
		return m;
	}
		
	@Override
	public double get(int i, int j){
		verifyBounds(i,j);
		if(map.containsKey(i) && map.get(i).containsKey(j)){
			return map.get(i).get(j);
		} else {
			return 0d;
		}
		
	}
		
	private void verifyBounds(int i, int j){
		if(i >= rows || j >= columns){
			throw new IllegalArgumentException("Matrix index out of bounds");
		}
	}
		
	@Override
	public void set(int i, int j, double value){
		verifyBounds(i,j);
		if(!map.containsKey(i)){
			map.put(i, new HashMap<Integer,Double>());
		}
		map.get(i).put(j, value);
	}
	
	private void updateWithOther(SparseMatrix other, BiFunction<Double,Double,Double> f){
		for(Integer i: other.map.keySet()){
			Map<Integer,Double> column = other.map.get(i);
			for(Integer j:column.keySet()){
				double thisValue = get(i,j);
				double otherValue = other.get(i, j);
				set(i, j, f.apply(thisValue, otherValue));
			}
		}
	}
	
	public void apply(Function<Double,Double> f){
		for(Integer i: map.keySet()){
			Map<Integer,Double> column = map.get(i);
			for(Integer j:column.keySet()){
				double thisValue = get(i,j);
				set(i, j, f.apply(thisValue));
			}
		}
	}
		
	@Override
	public void add(Matrix other){
		updateWithOther((SparseMatrix)other, (v1,v2) -> v1 + v2);
	}
		
	@Override
	public void subtract(Matrix other){
		updateWithOther((SparseMatrix)other, (v1,v2) -> v1 - v2);
	}
	
	@Override
	public void scalarMultiply(double factor){
		apply(v -> factor * v);
	}
	
	@Override
	public double getFrobeniusNorm(){
		double score = 0d;
		for(Integer i:map.keySet()){
			Map<Integer,Double> column = map.get(i);
			for(Integer j:column.keySet()){
				score += Math.pow(get(i,j),2);
			}
		}
		score = Math.sqrt(score);
		return score;
	}
	
	@Override
	public double[] getRow(int row){
		return getData()[row];
	}

	@Override
	public int getRowCount() {
		return rows;
	}

	@Override
	public int getColumnCount() {
		return columns;
	}
	
	
	
	
}
