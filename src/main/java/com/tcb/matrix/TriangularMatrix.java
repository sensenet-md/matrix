package com.tcb.matrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class TriangularMatrix implements Matrix {
	
	private final int dim;
	private final int size;
	private double[] data;

	public TriangularMatrix(int dim){
		this.dim = dim;
		this.size = index(dim-1,dim-1) + 1;
		this.data = new double[size];
	}
		
	protected int index(int i, int j){
		if(i>j){
			int tmp = i;
			i = j;
			j = tmp;
		}
		return i * (dim - 1)
				- ((i*i - i) / 2)
				+ j;
	}
	
	protected void verifyBounds(int i, int j){
		if (i>= dim || j>= dim) {
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	
	@Override
	public void set(int i, int j, double value){
		verifyBounds(i,j);
		data[index(i,j)] = value;
	}
	
	@Override
	public double get(int i, int j){
		verifyBounds(i,j);
		return data[index(i,j)];
	}
	
	
	@Override
	public void apply(Function<Double,Double> f){
		for(int i=0;i<size;i++){
			data[i] = f.apply(data[i]);
		}
	}

	@Override
	public double[][] getData() {
		double[][] result = new double[dim][dim];
		for(int i=0;i<dim;i++){
			for(int j=i;j<dim;j++){
				double v = get(i,j);
				result[i][j] = v;
				result[j][i] = v;
			}
		}
		return result;
	}

	@Override
	public Matrix copy() {
		TriangularMatrix c = new TriangularMatrix(dim);
		c.data = Arrays.copyOf(data, data.length);
		return c;
	}

	@Override
	public void add(Matrix other) {
		TriangularMatrix o = (TriangularMatrix)other;
		for(int i=0;i<size;i++){
			data[i] += o.data[i];
		}
	}

	@Override
	public void subtract(Matrix other) {
		TriangularMatrix o = (TriangularMatrix)other;
		for(int i=0;i<size;i++){
			data[i] -= o.data[i];
		}
	}

	@Override
	public void scalarMultiply(double factor) {
		this.apply((x) -> x * factor);
	}

	@Override
	public double getFrobeniusNorm() {
		double result = 0.0;
		for(int i=0;i<size;i++)
			result += 2. * Math.pow(data[i], 2);
		
		for(int i=0;i<dim;i++)
			result -= Math.pow(get(i,i), 2);
		
		result = Math.sqrt(result);
		return result;
	}

	@Override
	public double[] getRow(int row) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getRowCount() {
		return dim;
	}

	@Override
	public int getColumnCount() {
		return dim;
	}
	
	public int getDim(){
		return dim;
	}
	
}
