package com.tcb.matrix;

public interface LabeledMatrix<T> {
	public Double get(T a, T b);
	public T getLabel(Integer index);
	public Integer getIndex(T a);
	public void set(T a, T b, double value);
	public Matrix getMatrix();
	public int getRowCount();
	public int getColumnCount();
}
